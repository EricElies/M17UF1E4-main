﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;



public class HUD : MonoBehaviour
{
    public int avgFrameRate;
    public Text fps;
    

    void Update()
    {
        float current = 0;
        current = Time.frameCount / Time.time;
        avgFrameRate = (int)current;
        fps.text = avgFrameRate.ToString();
       
    }
}


