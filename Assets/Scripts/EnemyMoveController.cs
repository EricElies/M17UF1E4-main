﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMoveController : MonoBehaviour
{
    public Transform player;
    public float speed = 1.0f;
    private int score;
    private int lifes = 3;


    // Start is called before the first frame update

    void Start()
    {
        player = GameObject.Find("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {

        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, player.position, step);
    }



    void OnMouseDown()
    {

        if (this.gameObject.CompareTag("Enemy"))
        {
            score += 5;
            GameManager.Instance.scoreNew(score);
            Destroy(this.gameObject);
        }



    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            lifes = GameManager.Instance.lifesNew();
            Destroy(gameObject);
            if (lifes == 0)
            {   StartCoroutine(waiter());
                Destroy(collision.gameObject);
                
               //GameManager.Instance.transform.localRotation = Quaternion.Euler(180, 0, 0);
                //new WaitForSeconds(10500f);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

            }
        }
        if (collision.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator waiter()
    {
        GameManager.Instance.transform.localRotation = Quaternion.Euler(180, 0, 0);
        yield return new WaitForSeconds(400);


    }
}