﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    
    //CLASSE GAMEMANAGER
    private static GameManager _instance;
    /*public GameObject Player;
    public GUITextController GUITextController;

    public int Score;
    public int numEnemiesOnScene;*/

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    public int score = 0;
    public int lifes = 3;


    public int scoreNew(int scoreN) {
        score += scoreN;
        return score;
    }

    public int lifesNew()
    {
        lifes --;
        return lifes;
    }



    void Update()
    {
        GameObject.Find("Canvas").GetComponent<UIController>().Score.text = "Score " + score.ToString();
        GameObject.Find("Canvas").GetComponent<UIController>().playerLifes.text = "Lifes: " + lifes.ToString();
        GameObject.Find("Canvas").GetComponent<UIController>().EnemiesAlive.text = "Enemies: " + GameObject.FindGameObjectsWithTag("Enemy").Length;
    }
}
