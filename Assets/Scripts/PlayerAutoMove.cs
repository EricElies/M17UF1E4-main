﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAutoMove : MonoBehaviour
{
    private DataPlayer dataPlayer;
    private AnimationSprite animationSprite;
    public SpriteRenderer spriteRenderer;
    private Vector3 posR, posL, posI;
    private direction dir;

    // Start is called before the first frame update
    void Start()
    {
        dataPlayer = GetComponent<DataPlayer>();
        animationSprite = GetComponent<AnimationSprite>();
        dir = direction.left;
        posI = transform.position;
        posR = posI + new Vector3(dataPlayer.WalkDistance, 0);
        posL = posI - new Vector3(dataPlayer.WalkDistance, 0);
        animationSprite.initAnimation();
    }

    // Update is called once per frame
    void Update()
    {
        posR = posI + new Vector3(dataPlayer.WalkDistance, 0);
        posL = posI - new Vector3(dataPlayer.WalkDistance, 0);
        if (dir == direction.right) { Walk(posR); }
        else if (dir == direction.left) { Walk(posL); };
    }

    void Walk(Vector3 pos)
    {
        transform.position = Vector3.MoveTowards(transform.position, pos, dataPlayer.Speed);
        if (transform.position == pos)
        {
            dir = direction.stop;
            if (transform.position == posR)
            {
                dir = direction.left;
            }
            else
            {
                dir = direction.right;
            }
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }
}

enum direction { stop, right, left }
