﻿
﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;



public enum PlayerKind
{
    Knight,
    Sorcercer,
    Barbarian,
    Mage,
    Assassin,
    Bard
}

public class DataPlayer : MonoBehaviour
{


    // Save Sprite var to Spriterenderer


    [SerializeField]
    private string Name;
    public string Surname;
    public float Speed = 0.01f;
    public float Height;
    public float Weight = 0.05f;
    public float WalkDistance = 5f;
    public int animationSpriteIndex = 0;
    public PlayerKind Kind;
    public Sprite[] Sprites;
    public int lifes;
    // Start is called before the first frame update


    Vector3 End_pos;
    Vector3 Start_pos;

    public float x;
    public float y;
    public float z;
    public float way_there;
    public bool Derecha;
    bool q_presionada = false;


    void Awake()
    {
        QualitySettings.vSyncCount = 0;  // VSync must be disabled

        Application.targetFrameRate = 12;

    }
    void Start()
    {
        Start_pos = transform.position;
        End_pos = Start_pos + new Vector3(5, 0, 0);


        float Velocitat = (Speed / Weight) * Time.deltaTime;

    }

    // Update is called once per frame
    void Update()
    {

        if (!Derecha) MovimentoDerecha();
        else
        {
            MovimentoIzquierda();
        }
        AnimationSprite();

        if (Input.GetKeyDown(KeyCode.Q))
        {

            q_presionada = true;

        }
        else if (Input.GetKeyUp(KeyCode.Q)) { q_presionada = false; }
        if (q_presionada)
        {
            x += 0.01f * Time.deltaTime;
            y += 0.01f * Time.deltaTime;
            z += 0.01f * Time.deltaTime;
            transform.localScale = new Vector3(x, y, z);
            AnimationSprite();
        }

    }


    public void MovimentoDerecha()
    {
        //asignar-li la velocitat en funció del pes No em surt.
        //float Velocitat =  Speed * Time.deltaTime/Weight;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
        way_there += 0.01f;
        transform.position = Vector3.Lerp(Start_pos, End_pos, way_there);
        if (transform.position == End_pos)
        {
            //- Bonus: a l'acabar un recorregut, el personatge ha de parar, inclosa animació, un temps determinat. I després continua el seu patrullatge.
            Thread.Sleep(1000);

            Derecha = true;

        }
    }


    public void MovimentoIzquierda()
    {
        way_there -= 0.01f;
        transform.position = Vector3.Lerp(Start_pos, End_pos, way_there);
        transform.localRotation = Quaternion.Euler(0, 180, 0);
        if (transform.position == Start_pos)
        {
            //- Bonus: a l'acabar un recorregut, el personatge ha de parar, inclosa animació, un temps determinat. I després continua el seu patrullatge.
            Thread.Sleep(1000);
            Derecha = false;
        }
    }


    public void AnimationSprite()
    {
        SpriteRenderer spriteRendered = gameObject.GetComponent<SpriteRenderer>();

        if (animationSpriteIndex >= 0 && animationSpriteIndex <= 2)
        {
            spriteRendered.sprite = Sprites[animationSpriteIndex];
            animationSpriteIndex++;
        }
        else animationSpriteIndex = 0;
    }



}