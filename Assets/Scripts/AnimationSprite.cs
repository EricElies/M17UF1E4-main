﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSprite : MonoBehaviour
{
    public Sprite[] animationSprites;
    public SpriteRenderer spriteRenderer;
    private int spriteNum;
    private float frameT;

    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<DataPlayer>().Sprites;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteNum = 1;
        frameT = 1f / 12f;
        Application.targetFrameRate = (int)frameT;
    }

    void Update() { }

    private void nextFrame()
    {
        spriteRenderer.sprite = animationSprites[spriteNum];
        if (spriteNum == 2) { spriteNum = 0; }
        spriteNum++;
    }

    public void initAnimation() { InvokeRepeating("nextFrame", 0, frameT); }

    public void endAnimation()
    {
        CancelInvoke();
        spriteRenderer.sprite = animationSprites[1];
    }
}
