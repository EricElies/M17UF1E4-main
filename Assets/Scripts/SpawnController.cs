﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    
    float SpawnTime = 2f;
    float nextEnemy = 0.0f; 
    


    void Update()
    {
        if (Time.time > nextEnemy)
        {
            nextEnemy = Time.time + SpawnTime;
            Instantiate(enemy, new Vector2(transform.position.x, Random.Range(-4f, 4f)), Quaternion.identity);
        }
    }

    
}
